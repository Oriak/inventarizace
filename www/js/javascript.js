jQuery(function($){
jQuery('.usermenu').click(function() {
jQuery('.account-modal').css('display', 'grid');
jQuery('.notice').css('visibility', 'hidden');
jQuery('body').css('overflow', 'hidden');
});
jQuery('.acc-modal-close').click(function() {
jQuery('.account-modal').css('display', 'none');
jQuery('.notice').css('visibility', 'visible');
jQuery('body').css('overflow', 'initial');
});});

jQuery(function($){
jQuery('.mobile-menu-button').click(function() {
jQuery('.mobile-menu').css('display', 'grid');
jQuery('.notice').css('visibility', 'hidden');
jQuery('body').css('overflow', 'hidden');
});
jQuery('.mobile-menu-close').click(function() {
jQuery('.mobile-menu').css('display', 'none');
jQuery('.notice').css('visibility', 'visible');
jQuery('body').css('overflow', 'initial');
});})

jQuery(function($){
jQuery('.review-btn').click(function() {
jQuery('.review-modal').css('display', 'grid');
jQuery('.notice').css('visibility', 'hidden');
jQuery('body').css('overflow', 'hidden');
});
jQuery('.review-btn-close').click(function() {
jQuery('.review-modal').css('display', 'none');
jQuery('.notice').css('visibility', 'visible');
jQuery('body').css('overflow', 'initial');
});});

jQuery(function($){
jQuery('.poptat-btn').click(function() {
jQuery('.poptat-modal').css('display', 'grid');
jQuery('.notice').css('visibility', 'hidden');
jQuery('body').css('overflow', 'hidden');
});
jQuery('.poptat-btn-close').click(function() {
jQuery('.poptat-modal').css('display', 'none');
jQuery('.notice').css('visibility', 'visible');
jQuery('body').css('overflow', 'initial');
});});
function showPoptatModal(id){
    jQuery('.poptat-modal'+id).css('display', 'grid');
    jQuery('.notice').css('visibility', 'hidden');
    jQuery('body').css('overflow', 'hidden');
}

jQuery(function($){
jQuery('.dotaz-btn').click(function() {
jQuery('.dotaz-modal').css('display', 'grid');
jQuery('.notice').css('visibility', 'hidden');
jQuery('body').css('overflow', 'hidden');
});
jQuery('.dotaz-btn-close').click(function() {
jQuery('.dotaz-modal').css('display', 'none');
jQuery('.notice').css('visibility', 'visible');
jQuery('body').css('overflow', 'initial');
});});

$(".show-more-articles").on('click', function (e) {
     e.preventDefault();
     $(".blog-list .gallery-cell.hidden").slice(0,3).removeClass("hidden");
});
