<?php

namespace App\Utils\Borrow;

use Nette\Database\Context;


class BorrowService
{
        /** @var \Nette\Database\Context */
        protected $db;

        public function __construct(Context $db)
        {
                $this->db = $db;
        }

        public function getBorrows($itemId){
                return $this->db->table('borrow')
                                ->where('item_id', $itemId)
                                ->order('borrowed_date DESC');
        }
}