<?php

namespace App\PublicModule\Presenters;

use App\Presenters\BasePresenter;
use Nette\Application\UI\Presenter;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;

use Nette\Database\Context;
/**
 * Základní presenter pro všechny ostatní presentery v CoreModule.
 * @package App\CoreModule\Presenters
 */
class BorrowPresenter extends Presenter
{
    /** @var \Nette\Database\Context @inject */
    public $db;

    private $item;
    private $borrow;

    protected function startup(){
        parent::startup();
        $this->user->getStorage()->setNamespace('Public');
    }

    // NAČTENÍ POLOŽKY
    public function actionDefault($token){
      $item = null;
      if($token){
        $item = $this->db->table('item')->where('token', $token)->fetch();
        $this->item = $item;
        if($item){
          $borrow = $this->db->table('borrow')->where('item_id', $item->id)->where('borrowed_date <= CURDATE() AND returned_date IS NULL')->order('return_date DESC')->fetch();
          $this->borrow = $borrow;
        }
      }
      $this->template->item = $item;
      $this->template->borrow = $borrow ? $borrow : NULL;

      // order will be CREATED in another script
    }

    protected function createComponentForm(){
      $date = strtotime("+7 day");

      $form = new Form;
      $form->setRenderer(new BootstrapRenderer);

      $form->addText('borrowed_name', 'Jméno')
              ->setAttribute('class', 'form-control')
              ->setRequired();

      $form->addEmail('borrowed_email', 'Email')
              ->setAttribute('class', 'form-control')
              ->setRequired();

      $form->addText('borrowed_date', 'Půjčeno od')
            ->setDefaultValue(date('Y-m-d'))
            ->setRequired('Vyplňte datum Půjčeno od:')
            ->setType('date')
            ->setAttribute('class', 'form-control');
      $form->addText('return_date', 'Vrátit do')
            ->setDefaultValue(date('Y-m-d', $date))
            ->setRequired('Vyplňte datum Vrátit do:')
            ->setType('date')
            ->setAttribute('class', 'form-control');

      $form->addSubmit('save', 'Uložit');

      if($this->borrow){
        $form['borrowed_name']->setDisabled();
        $form['borrowed_email']->setDisabled();
        $form['borrowed_date']->setDisabled();
        $form['return_date']->setDisabled();
        $form['save']->setDisabled();
      }
      $form->onSuccess[] = [$this, 'save'];
      return $form;
    }

    protected function createComponentFormReturn(){

      $form = new Form;
      $form->setRenderer(new BootstrapRenderer);

      $form->addEmail('borrowed_email', 'Email')
              ->setAttribute('class', 'form-control')
              ->setRequired();

      $form->addSubmit('save', 'Potvrdit vrácení');

      $form->onSuccess[] = [$this, 'saveReturn'];
      return $form;
    }

    public function save(Form $form)
    {
        $values = $form->getValues();
        $post = $this->db->query('INSERT INTO borrow', [
            'item_id'            =>  $this->item->id,
            'borrowed_name'         => $values->borrowed_name,
            'borrowed_email'         => $values->borrowed_email,
            'borrowed_date' => $values->borrowed_date,
            'return_date' => $values->return_date,
            'qty'  => 1,
            'note'  => "[" . date('Y-m-d H:i:s') . "] Půjčeno přes QR kód"
        ]);

        $this->flashMessage("Nastavení uloženo", "success");
        $this->redirect('this');
    }

    public function saveReturn(Form $form){
        $values = $form->getValues();
        if($this->borrow){
            if($this->borrow->borrowed_email == $values->borrowed_email){
                $post = $this->db->table('borrow')->get($this->borrow->id);
                $post->update(['returned' => 1, 'returned_date' => date('Y-m-d H:i:s'), 'note' => $this->borrow->note . "\n[" . date('Y-m-d H:i:s') . "] Vráceno přes QR kód"]);
                $this->flashMessage("Předmět byl úspěšně vrácen!", "success");
                $this->redirect('this');
            }
            else{
                $this->flashMessage("Vrácení se nezdařilo - byl zadán nesprávný email uvedený při výpůjčce!", "danger");
            }
        }
        else{
            $this->flashMessage("Vrácení se nezdařilo - výpůjčka nebyla nalezena!", "danger");
        }
    }
    /*

    */


}
