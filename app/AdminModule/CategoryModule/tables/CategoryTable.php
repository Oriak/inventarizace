<?php

/**
 * Category table
 */

namespace Category;

use Nette\Database\Table\Selection;
use Nette\Database\Context;
use Nette\Application\UI\Control;

class CategoryTable extends Control
{
                /** @var \Nette\Database\Context */
	protected $db;

                public function __construct(Context $db)
	{
		$this->db = $db;
	}

	public function render() {
                                $this->template->setFile(__DIR__ . '/table.latte');
                                $this->template->data = $this->getModel();
                                $this->template->render();
                }

	/**
	 * @return \Nette\Database\Table\Selection
	 */
	private function getModel()
	{
		return $this->db->table('category');
	}


}

interface ICategoryTable
{
	/**
	 * @return \Category\CategoryTable
	 */
	public function create(): CategoryTable;
}
