<?php

namespace App\AdminModule\CategoryModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Category\ICategoryTable;
use Category\ICategoryFormFactory;
use Nette\Application\UI\Form;
use Nette\Database\Context;

/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class CategoryPresenter extends BaseAdminPresenter
{
        
                /** @var \Category\ICategoryTable */
                private $categoryTable;
                
                /** @var \Category\ICategoryFormFactory */
                private $categoryForm;
                
                /** @var \Nette\Database\Context */
                protected $db;
        
    
                public function __construct(Context $db, ICategoryTable $categoryTable, ICategoryFormFactory $categoryForm)
	{
		parent::__construct();
                                $this->categoryTable = $categoryTable;
                                $this->categoryForm = $categoryForm;
                                $this->db = $db;
	}

	public function startup()
	{
		parent::startup();
	}
                
                protected function createComponentTable(){
                                return $this->categoryTable->create();
                }
                
                protected function createComponentForm()
                {
                                $formAdd = $this->categoryForm->create(null);
                                $formAdd->onSuccess[] = function(Form $form) use ($formAdd) {
                                        if($form['basic']->getValues()->id){
                                            $this->flashMessage('Kategorie upravena', 'success');
                                            $this->redirect('Category:');
                                        }
                                        else{
                                            $this->flashMessage('Kategorie přidána', 'success');
                                            $this->redirect('Category:');
                                        }
                                };
                                return $formAdd;
                }

                public function actionEdit($id)
                {
                        $this->categoryForm->create($id);
                        $this['form']->load($id);
                }

                public function actionDelete($id){
                                $this->db->table('category')->where('id', $id)->delete();
                                $this->flashMessage('Kategorie vymazána', 'warning');
                                $this->redirect('Category:');
                }
                
                
        
        
        
        
        
}
