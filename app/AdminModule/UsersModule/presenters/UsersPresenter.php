<?php

namespace App\AdminModule\UsersModule\Presenters;

use Nette\Application\BadRequestException;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Nette\Database\Context;
use Nette\Application\UI;
use Nette\Application\UI\Form;
use AdminModule\components\UsersFormFactory\UsersFormFactory;
use AdminModule\Users\UsersGrid;


/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class UsersPresenter extends BaseAdminPresenter
{
        
        /** @var \Nette\Database\Context */
	protected $db;
        
        private $usersFormFactory;
        private $usersGridFactory;
    
        public function __construct(Context $db, UsersFormFactory $usersFormFactory, UsersGrid $usersGridFactory)
	{
		parent::__construct();
		$this->db = $db;
                $this->usersFormFactory = $usersFormFactory;
                $this->usersGridFactory = $usersGridFactory;
	}

	public function startup()
	{
		parent::startup();
                $this->template->teams = $this->getUsers();
		/*
		$user = $this->em->getRepository(User::class)->find(1);
		$user->setPassword($this->passwordService->hash('heslo'));

		$this->em->persist($user);
		$this->em->flush();
		*/
	}
        
        protected function createComponentForm()
        {
            $formAdd = $this->usersFormFactory->create();
            $formAdd->onSuccess[] = function(Form $form) use ($formAdd) {
                    if($form->getValues()->id){
                        $this->flashMessage('Uživatel upraven', 'success');
                        $this->redirect('Users:');
                    }
                    else{
                        $this->flashMessage('Uživatel přidán', 'success');
                        $this->redirect('Users:');
                    }
            };
            return $formAdd;
        }
        
        public function actionEdit($id)
        {
            $this['form']->setDefaults($this->getUserById($id)->toArray());
        }
        
        public function actionDelete($id){
            $this->db->table('user')->where('id', $id)->delete();
            $this->flashMessage('Uživatel vymazán', 'warning');
            $this->redirect('Users:');
        }


        protected function createComponentGrid(){
            return $this->usersGridFactory->create();
        }
        
        
        
        private function getUsers(){
            return $this->db->table('user');
        }
        
        private function getUserById($id){
            return $this->db->table('user')->get($id);
        }
        
        
}
