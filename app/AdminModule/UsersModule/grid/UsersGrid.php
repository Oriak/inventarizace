<?php

/**
 * Event grid
 */

namespace AdminModule\Users;

use Ublaboo\DataGrid\DataGrid;
use Nette\Database\Table\Selection;
use Nette\Database\Context;

class UsersGrid 
{
        /** @var \Nette\Database\Context */
	protected $db;
        
        public function __construct(Context $db)
	{
		$this->db = $db;
	}

	/**
	 * @return Grid
	 */
	public function create()
	{
		$grid = new DataGrid();
                $grid->setDataSource($this->getModel());
                $grid->setItemsPerPageList([30, 50, 100, 500, 'all']);
                    
                /**
                * Columns
                */
               $grid->addColumnNumber('id', 'Id')
                       ->addAttributes(["width" => "8%"])
                       ->setSortable()
                        ->setAlign('left');
               
               $grid->addColumnText('name', 'Jméno')
                    ->setSortable()
                    ->addAttributes(['class' => 'text-center']);
               
               $grid->addColumnText('surname', 'Příjmení')
                    ->setSortable()
                    ->addAttributes(['class' => 'text-center']);
               
               $grid->addColumnText('login', 'Login')
                    ->setSortable()
                    ->addAttributes(['class' => 'text-center']);
               
               $grid->addColumnText('email', 'Email')
                    ->setSortable()
                    ->addAttributes(['class' => 'text-center']);
               
               /**
                * Filters
                */
               $grid->addFilterText('name', 'Search', ['name']); 
               $grid->addFilterText('surname', 'Search', ['surname']); 
               $grid->addFilterText('login', 'Search', ['login']); 
               $grid->addFilterText('email', 'Search', ['email']); 

               /**
                * ACtions
                */
               $grid->addAction('edit', 'Upravit', '')
                       ->setClass('btn btn-xs btn-primary ajax');
               
               $grid->addAction('delete', '', '')
                    ->setIcon('trash')
                    ->setTitle('Delete')
                    ->addAttributes(['onclick' => "return confirm('Opravdu chcete smazat vybraného uživatele?');"])
                    ->setClass('btn btn-xs btn-danger ajax')
                    ->setConfirm('Určitě chcete smazat tým?', '');
		
		return $grid;
	}

	/**
	 * @return \Nette\Database\Table\Selection
	 */
	private function getModel()
	{
		return $this->db->table('user');
	}


}

