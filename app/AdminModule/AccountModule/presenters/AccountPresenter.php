<?php

namespace App\AdminModule\AccountModule\Presenters;

use Nette\Application\BadRequestException;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Nette\Database\Context;
use Nette\Application\UI;
use Nette\Application\UI\Form;
use AdminModule\components\IAccountFormFactory;
use AdminModule\Account\AccountGrid;


/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class AccountPresenter extends BaseAdminPresenter
{

        /** @var \Nette\Database\Context */
        protected $db;

        private $accountFormFactory;
        private $accountGridFactory;

        public function __construct(Context $db, IAccountFormFactory $accountFormFactory, AccountGrid $accountGridFactory)
        {
                parent::__construct();
                $this->db = $db;
                $this->accountFormFactory = $accountFormFactory;
                $this->accountGridFactory = $accountGridFactory;
        }

        public function startup()
        {
                parent::startup();
                $this->template->teams = $this->getUsers();
                /*if(!$this->user->isAllowed('account')){
                        $this->flashMessage('Nemáte dostatečná oprávnění!', 'danger');
                        $this->redirect(':Admin:Dashboard:Homepage:');
                }*/
        }

        protected function createComponentForm()
        {
            $formAdd = $this->accountFormFactory->create(null);
            $formAdd->onSuccess[] = function(Form $form) use ($formAdd) {
                    if($form['basic']->getValues()->id){
                        $this->flashMessage('Uživatel upraven', 'success');
                        $this->redirect('Account:');
                    }
                    else{
                        $this->flashMessage('Uživatel přidán', 'success');
                        $this->redirect('Account:');
                    }
            };
            return $formAdd;
        }

        public function actionEdit($id)
        {
            $this->accountFormFactory->create($id);
            $this['form']->load($id);
        }

        public function actionDelete($id){
            if((int)$id !== $this->user->getId()){
              $this->db->table('account')->where('id', $id)->delete();
              $this->flashMessage('Uživatel vymazán', 'warning');
              $this->redirect('Account:');
            }
            else{
              $this->flashMessage('Nelze smazat sám sebe!', 'danger');
              $this->redirect('Account:');
            }

        }


        protected function createComponentGrid(){
            return $this->accountGridFactory->create();
        }



        private function getUsers(){
            return $this->db->table('account');
        }

        private function getUserById($id){
            return $this->db->table('account')->get($id);
        }


}
