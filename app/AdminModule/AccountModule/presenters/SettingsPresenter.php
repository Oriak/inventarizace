<?php

namespace App\AdminModule\AccountModule\Presenters;

use Nette\Application\BadRequestException;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Nette\Database\Context;
use Nette\Application\UI;
use Nette\Application\UI\Form;
use AdminModule\components\ISettingsFormFactory;
use AdminModule\Account\AccountGrid;


/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class SettingsPresenter extends BaseAdminPresenter
{
        
        /** @var \Nette\Database\Context */
	protected $db;
        
        private $settingsFormFactory;
        private $accountGridFactory;
    
        public function __construct(Context $db, ISettingsFormFactory $settingsFormFactory, AccountGrid $accountGridFactory)
	{
		parent::__construct();
		$this->db = $db;
                $this->settingsFormFactory = $settingsFormFactory;
                $this->accountGridFactory = $accountGridFactory;
	}

	public function startup()
	{
		parent::startup();
                //$this->template->teams = $this->getUsers();
		/*
		$user = $this->em->getRepository(User::class)->find(1);
		$user->setPassword($this->passwordService->hash('heslo'));

		$this->em->persist($user);
		$this->em->flush();
		*/
	}
        
        protected function createComponentForm()
        {
            $formAdd = $this->settingsFormFactory->create();
            $formAdd->onSuccess[] = function(Form $form) use ($formAdd) {
                if($form->getValues()->id){
                    $this->flashMessage('Uživatel upraven', 'success');
                    $this->redirect('Settings:');
                }
                else{
                    $this->flashMessage('Uživatel přidán', 'success');
                    $this->redirect('Settings:');
                }
            };
            $formAdd->onError[] = function($message) use ($formAdd) {
                    $this->flashMessage($message, 'danger');
                    $this->redirect('Settings:');
            };
            return $formAdd;
        }
        
        public function actionDefault()
        {
            $this['form']->load();
        }
        
        public function actionDelete($id){
            $this->db->table('account')->where('id', $id)->delete();
            $this->flashMessage('Uživatel vymazán', 'warning');
            $this->redirect('Account:');
        }


        protected function createComponentGrid(){
            return $this->accountGridFactory->create();
        }
        
        
        
        
        
        
}
