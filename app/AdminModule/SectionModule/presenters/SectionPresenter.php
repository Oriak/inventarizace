<?php

namespace App\AdminModule\SectionModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Section\ISectionTable;
use Section\ISectionFormFactory;
use Nette\Application\UI\Form;
use Nette\Database\Context;

/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class SectionPresenter extends BaseAdminPresenter
{
        
                /** @var \Section\ISectionTable */
                private $sectionTable;
                
                /** @var \Section\ISectionFormFactory */
                private $sectionForm;
                
                /** @var \Nette\Database\Context */
                protected $db;
        
    
                public function __construct(Context $db, ISectionTable $sectionTable, ISectionFormFactory $sectionForm)
	{
		parent::__construct();
                                $this->sectionTable = $sectionTable;
                                $this->sectionForm = $sectionForm;
                                $this->db = $db;
	}

	public function startup()
	{
		parent::startup();
	}
                
                protected function createComponentTable(){
                                return $this->sectionTable->create();
                }
                
                protected function createComponentForm()
                {
                                $formAdd = $this->sectionForm->create(null);
                                $formAdd->onSuccess[] = function(Form $form) use ($formAdd) {
                                        if($form->getValues()->id){
                                            $this->flashMessage('Sekce upravena', 'success');
                                            $this->redirect('Section:');
                                        }
                                        else{
                                            $this->flashMessage('Sekce přidána', 'success');
                                            $this->redirect('Section:');
                                        }
                                };
                                return $formAdd;
                }

                public function actionEdit($id)
                {
                        $this->sectionForm->create($id);
                        $this['form']->load($id);
                }

                public function actionDelete($id){
                                $this->db->table('section')->where('id', $id)->delete();
                                $this->flashMessage('Sekce vymazána', 'warning');
                                $this->redirect('Section:');
                }
                
                private function getSectionById($id){
                        return $this->db->table('section')->get($id);
                }
        
        
        
        
        
}
