<?php

namespace App\AdminModule\Presenters;

use Contributte\MenuControl\UI\IMenuComponentFactory;
use Contributte\MenuControl\UI\MenuComponent;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Presenter;
use Nette\Application\BadRequestException;

/**
 * Základní presenter pro všechny ostatní presentery v CoreModule.
 * @package App\CoreModule\Presenters
 */
abstract class BaseAdminPresenter extends Presenter
{
        
        /** @var \Contributte\MenuControl\UI\IMenuComponentFactory @inject */
        private $menuFactory;
        
        public function injectBasePresenter(IMenuComponentFactory $menuFactory)
        {
                        $this->menuFactory = $menuFactory;
        }
        
        protected function createComponentMenu(): MenuComponent
        {
                        return $this->menuFactory->create('admin');
        }
        
        protected function startup(){
                        parent::startup();
                        $this->user->getStorage()->setNamespace('Admin');
                        $this->template->user = $this->user->getIdentity();
                        $this->checkLogin();
        }
        
        protected function checkLogin() {
                        if (!$this->user->loggedIn) {
                                $this->redirectSign();
                        }
        }
        
        public function handleOut() {
                        $this->user->logout();
                        $this->redirect("this");
        }
        
        public function redirectSign() {
                        $restoreKey = $this->storeRequest();
                        $this->redirect(':Admin:Login:Sign:in', ['restoreKey' => $restoreKey]);
        }
        
        public function redirectHomepage() {
                        $this->redirect(':Admin:Dashboard:Homepage:');
        }
	
}
