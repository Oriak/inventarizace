<?php

namespace Section;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Security\Passwords;
use Nette\Application\UI\Control;

class SectionFormFactory extends Control
{

        /** @var \Nette\Database\Context */
        protected $db;

        private $id;

        public $onError = [];

        public $onSuccess = [];

        public function __construct($id, Context $db)
        {
                $this->id = $id;
                $this->db = $db;
        }

        /**
         * @return Form
         */
        protected function createComponentForm()
        {
                        $form = new Form;
                        $form->addHidden('id');
                        $form->addText('name', 'Název sekce: ')
                             ->setRequired();

                        $form->addSubmit('save', 'Uložit');
                        $form->onSuccess[] = [$this, 'save'];
                        return $form;
        }

        public function render() {
                        $this->template->setFile(__DIR__ . '/form.latte');
                        $this->template->render();
        }

        public function load($id){
                        $this->id = $id;
                        $this->template->id = $id;
                        $this['form']->setDefaults($this->getSectionById($id)->toArray());
        }


        public function save(Form $form)
        {
                        $values = $form->getValues();
                        if ($values->id) {
                            $post = $this->db->table('section')->get($values->id);
                            $post->update($values);
                        } else {
                            $values->id = NULL;
                            $post = $this->db->table('INSERT INTO section', $values);
                        }
                        $this->onSuccess($form);
        }

        private function getSectionById(){
                        return $this->db->table('section')->get($this->id);
        }

}

interface ISectionFormFactory
{
	/**
                 * @param $id
	 * @return \Section\SectionFormFactory
	 */
	public function create($id): SectionFormFactory;
}
