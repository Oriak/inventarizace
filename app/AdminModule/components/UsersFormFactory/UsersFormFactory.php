<?php

namespace AdminModule\components\UsersFormFactory;

use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;
use Nette\Database\Context;
use Nette\Security\Passwords;
use App\Model\UserManager;

class UsersFormFactory 
{
    
    /** @var \Nette\Database\Context */
    protected $db;
    
    /** @var \App\Model\UserManager @inject */
    private $userManager;
    
    private $id = null;
    
    public function __construct(Context $db, UserManager $userManager)
    {
            $this->db = $db;
            $this->userManager = $userManager;
    }
    
    /**
     * @return Form
     */
    public function create($id = null)
    {
        if($id){
            $this->id = $id;
        }
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer);
        $form->addHidden('id');
        $form->addText('name', 'Jméno:')
             ->setRequired();
        $form->addText('surname', 'Příjmení:')
             ->setRequired();
        $form->addText('login', 'Login:')
             ->setRequired();
        $form->addEmail('email', 'Email:')
             ->setRequired();
        /*$form->addPassword('password', 'Heslo:')
             ->setRequired();
        $form->addPassword('password_repeat', 'Heslo znovu:')
             ->setRequired();*/
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = [$this, 'save'];
        return $form;
    }
    
    
    public function save(Form $form)
    {
        $values = $form->getValues();
            if ($values->id) {
                $post = $this->db->table('account')->get($values->id);
                $post->update($values);
            } else {
                $values->id = NULL;
                $password = $this->userManager->randomPassword();
                $values->password = Passwords::hash($password);
                $post = $this->db->query('INSERT INTO account', $values);
                
                
            }
    }
}
