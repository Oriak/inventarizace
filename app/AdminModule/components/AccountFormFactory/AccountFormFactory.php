<?php

namespace AdminModule\components;

use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;
use Nette\Database\Context;
use Nette\Security\Passwords;
use App\Model\UserManager;
use Nette\Utils\Strings;
use Nette\Utils\Random;
use Nette\Application\UI\Control;
use Nextras\Forms\Controls\DatePicker;

class AccountFormFactory extends Control
{

        /** @var \Nette\Database\Context */
        protected $db;

        /** @var \App\Model\UserManager @inject */
        private $userManager;

        private $passwords;

        public $onError = [];

        public $onSuccess = [];

        private $id;

        public function __construct($id,Context $db, UserManager $userManager, Passwords $passwords)
        {
                $this->id = $id;
                $this->db = $db;
                $this->userManager = $userManager;
                $this->passwords = $passwords;
        }

        /**
         * @return Form
         */
        protected function createComponentForm()
        {
                $form = new Form;
                $form->setRenderer(new BootstrapRenderer);
                $basic = $form->addContainer('basic');
                $basic->addHidden('id');
                $basic->addText('name', 'Jméno *')
                        ->setAttribute('class', 'form-control')
                        ->setRequired();
                $basic->addText('surname', 'Příjmení *')
                        ->setAttribute('class', 'form-control')
                        ->setRequired();
                $basic->addText('login', 'Login *')
                        ->setAttribute('class', 'form-control')
                        ->setRequired();
                $basic->addEmail('email', 'Email:')
                        ->setAttribute('class', 'form-control');

                $basic->addPassword('password', 'Heslo:')
                        ->setAttribute('class', 'form-control');
                $basic->addPassword('password_repeat', 'Heslo znovu:')
                        ->setAttribute('class', 'form-control');

                $form->addSubmit('save', 'Uložit');
                $form->onSuccess[] = [$this, 'save'];
                return $form;
        }


        public function save(Form $form)
        {
                $values = $form['basic']->getValues();
                if($values->password){
                        if($values->password === $values->password_repeat){
                                $values->password = $this->passwords->hash($values->password);
                                unset($values->password_repeat);
                        }
                        else{
                                return $this->onError('Hesla nesouhlasí!');
                        }
                }
                else{
                        unset($values->password);
                        unset($values->password_repeat);
                }
                if ($values->id) {
                    $post = $this->db->table('account')->get($values->id);
                    $post->update($values);
                    $accountId = $values->id;
                } else {
                    $values->id = NULL;
                    $post = $this->db->query('INSERT INTO account', $values);
                    $accountId = $this->db->getInsertId();
                }
                $this->onSuccess($form);
        }

        public function render() {
                $this->template->setFile(__DIR__ . '/form.latte');
                $this->template->render();
        }

        public function load($id){
                $this->id = $id;
                $this['form']['basic']->setDefaults($this->getData($id)->toArray());
        }

        private function getData($id){
                return $this->db->table('account')->get($id);
        }

        private function getRoles(){
                $return = [];
                foreach($this->db->table('role') as $row){
                    $return[$row->id] = $row->name;
                }
                return $return;
        }

        private function getBranches(){
                $return = [];
                foreach ($this->db->table('branches') as $row){
                    $return[$row->id] = $row->name;
                }
                return $return;
        }

        private function getAccountBranches(){
                $return = [];
                $selection = $this->db->table('branches_has_account')->select('branches_id')->where('account_id', $this->id);
                foreach($selection as $row){
                    $return[$row->branches_id] = $row->branches_id;
                }
                return $return;
        }




}

interface IAccountFormFactory
{
	/**
         * @param int id
	 * @return \AdminModule\components\AccountFormFactory
	 */
	public function create($id): AccountFormFactory;
}
