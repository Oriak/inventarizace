<?php

namespace AdminModule\components;

use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;
use Nette\Database\Context;
use Nette\Security\Passwords;
use App\Model\UserManager;
use Nette\Security\User;
use Nette\Application\UI\Control;

class SettingsFormFactory extends Control
{

    /** @var \Nette\Database\Context */
    protected $db;

    /** @var \App\Model\UserManager @inject */
    private $userManager;

    private $user;


    public $onError = [];

    public $onSuccess = [];

    public function __construct(Context $db, UserManager $userManager, User $user)
    {
            $this->db = $db;
            $this->userManager = $userManager;
            $this->user = $user;
    }



    private function getRoles(){
        $return = [];
        foreach($this->db->table('role') as $row){
            $return[$row->id] = $row->name;
        }
        return $return;
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/settings.latte');
        $this->template->accountData = $this->getUserData();
        $this->template->render();
    }

    public function load(){
        $this['formSettings']->setDefaults($this->getUserData()->toArray());
    }

    private function getUserData(){
        return $this->db->table('account')->get($this->user->getIdentity()->getId());
    }

    protected function createComponentFormSettings()
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer);
        $form->addHidden('id');
        $form->addText('name', 'Jméno:')
             ->setRequired();
        $form->addText('surname', 'Příjmení:')
             ->setRequired();
        $form->addText('title', 'Titul');
        $form->addText('login', 'Login:');
        $form->addEmail('email', 'Email:');

        $form->addSelect('role', 'Role', $this->getRoles())
             ->setRequired();

         $form->addCheckbox('changePass', 'Chci změnit heslo')
            ->addCondition($form::EQUAL, true)
            ->toggle('setPass')
            ->toggle('setPass2');
        $form->addPassword('password', 'Heslo:')
             ->setOption('id', 'setPass');
        $form->addPassword('password_repeat', 'Heslo znovu:')
             ->setOption('id', 'setPass2');

        $form->addUpload('image', 'Obrázek (uložení nového obrázku způsobí ztrátu původního)')
             ->setRequired(false)
             ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 3 MB.', 3 * 1024 * 1024);

        $form->addTextArea('about', 'Popis uživatele')
             ->setAttribute('class', 'mceEditor');

        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = function (Form $form){
            $this->saveSettings($form);
        };
        return $form;
    }

    private function saveSettings(Form $form)
    {
        $values = $form->getValues();
        $image = $values->image;
        unset($values->image);
        if($values->changePass !== false){
            if($values->password === $values->password_repeat){
                $values->password= Passwords::hash($values->password);
            }
            else{
                return $this->onError('Hesla nesouhlasí!');
            }
            unset($values->changePass);
            unset($values->password_repeat);
        }
        else{
            unset($values->changePass);
            unset($values->password);
            unset($values->password_repeat);
        }
        if ($values->id) {
            $post = $this->db->table('account')->get($values->id);
            if($image->getSize() > 0){
                $existFile = $this->db->table('account')->where('id', $values->id)->fetch();
                if($existFile->image){
                    unlink(WWW_DIR . '/' . $existFile->image);
                }
                $folder = WWW_DIR . "/data/account/" . $values->id;
                if(!file_exists($folder)){
                    mkdir($folder, 0777, true);
                }
                $path = $folder . "/" . $image->getSanitizedName();
                $image->move($path);
                $values->image = 'data/account/' . $values->id . '/' . $image->getSanitizedName();

            }
            $post->update($values);
        } else {
            $values->id = NULL;
            $values->password = $this->userManager->randomPassword();
            $values->password= Passwords::hash($values->password);
            $post = $this->db->query('INSERT INTO account', $values);
        }
        $this->onSuccess($form);
    }
}

interface ISettingsFormFactory
{
	/**
	 * @return \AdminModule\components\SettingsFormFactory
	 */
	public function create(): SettingsFormFactory;
}
