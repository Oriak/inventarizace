<?php

namespace Item;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Security\Passwords;
use Nette\Application\UI\Control;
use Nette\Security\User;

class BorrowFormFactory extends Control
{

    /** @var \Nette\Database\Context */
    protected $db;

    private $id;

    /** @var \Nette\Security\User */
    private $user;

    public $onError = [];

    public $onSuccess = [];

    public function __construct($id, Context $db, User $user)
    {
        $this->id = $id;
        $this->db = $db;
        $this->user = $user;
    }

    /**
     * @return Form
     */
    protected function createComponentForm()
    {
        $date = strtotime("+7 day");

        $form = new Form;
        $basic = $form->addContainer('basic');
        $basic->addHidden('id');
        $basic->addText('borrowed_name', 'Vypůjčeno komu: ')
             ->setRequired();
        $basic->addText('borrowed_email', 'Email: ')
             ->setRequired();
        $basic->addText('qty', 'Množství: ')->setHtmlType('number');
        $items = $form->addContainer('items');
        $items->addMultiSelect('item_select', 'Půjčené položky', $this->getItems());
        $items->addText('borrowed_date', 'Půjčeno od')->setDefaultValue(date('Y-m-d'))->setRequired('Vyplňte datum Půjčeno od:')->setType('date');
        $items->addText('return_date', 'Vrátit do')->setDefaultValue(date('Y-m-d', $date))->setRequired('Vyplňte datum Vrátit do:')->setType('date');

        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = [$this, 'save'];
        return $form;
    }

    protected function createComponentFormEdit()
    {
        $date = strtotime("+7 day");

        $form = new Form;
        $basic = $form->addContainer('basic');
        $basic->addHidden('id');
        $basic->addText('borrowed_name', 'Vypůjčeno komu: ')
             ->setRequired();
        $basic->addText('borrowed_email', 'Email: ')
             ->setRequired();
        $basic->addText('note', 'Poznámka: ');
        $basic->addText('qty', 'Množství: ')->setHtmlType('number')
             ->setRequired();
        $items = $form->addContainer('items');
        $items->addSelect('item_select', 'Půjčené položky', $this->getItems());
        $items->addText('borrowed_date', 'Půjčeno od')->setRequired('Vyplňte datum Půjčeno od:')->setType('date');
        $items->addText('return_date', 'Vrátit do')->setRequired('Vyplňte datum Vrátit do:')->setType('date');

        $form->addSubmit('saveEdit', 'Uložit');
        $form->addSubmit('returned', 'Označit jako vráceno');
        $form->addSubmit('notReturned', 'Označit jako NEVRÁCENO');
        $form->onSuccess[] = [$this, 'saveEdit'];
        return $form;
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/formBorrow.latte');
        $this->template->id = $this->id;
        $this->template->render();
    }

    public function load($id){
        $this->id = $id;
        $this['formEdit']['basic']->setDefaults($this->getBorrowById($id)->toArray());
        $this['formEdit']['items']['borrowed_date']->setDefaultValue(date("Y-m-d", strtotime($this->getBorrowById($id)->borrowed_date)));
        $this['formEdit']['items']['return_date']->setDefaultValue(date("Y-m-d", strtotime($this->getBorrowById($id)->return_date)));
    }


    public function save(Form $form)
    {
        $values = $form['basic']->getValues();
        $items = $form['items']->getValues();
        foreach($items->item_select as $row){
            $post = $this->db->query('INSERT INTO borrow', [
                'item_id'            =>  $row,
                'borrowed_name'         => $values->borrowed_name,
                'borrowed_email'         => $values->borrowed_email,
                'borrowed_date' => $items->borrowed_date,
                'return_date' => $items->return_date,
                'qty'  => $values->qty
                /*'note'  => $values->note,
                */
            ]);
            $borrowId = $this->db->getInsertId();
            $this->db->query('INSERT INTO borrow_changes', ['account_id' => $this->user->getId(), 'borrow_id' => $borrowId]);
        }
        $this->onSuccess($form);
    }

    public function saveEdit(Form $form)
    {
        if($form->isSubmitted()->getName() == 'saveEdit'){
                $values = $form['basic']->getValues();
                $item = $form['items']->getValues();
                $this->db->table('borrow')->where('id', $values->id)->update([
                    'item_id'            =>  $item->item_select,
                    'borrowed_name'         => $values->borrowed_name,
                    'borrowed_email'         => $values->borrowed_email,
                    'borrowed_date' => $item->borrowed_date,
                    'return_date' => $item->return_date,
                    'note'  => $values->note,
                    'qty'  => $values->qty

                ]);
                $this->db->query('INSERT INTO borrow_changes', ['account_id' => $this->user->getId(), 'borrow_id' => $values->id]);
                $this->onSuccess($form);
        }
        elseif ($form->isSubmitted()->getName() == 'returned'){
                $values = $form['basic']->getValues();
                $post = $this->db->table('borrow')->get($values->id);
                $post->update(['returned' => 1, 'returned_date' => date('Y-m-d H:i:s'), 'note' => $values->note, 'qty' => $values->qty]);
                $this->db->query('INSERT INTO borrow_changes', ['account_id' => $this->user->getId(), 'borrow_id' => $values->id]);
                //$this->db->table('item_has_borrow')->where('borrow_id', $borrowId)->delete();
                $this->onSuccess($form);
        }
        elseif ($form->isSubmitted()->getName() == 'notReturned'){
                $values = $form['basic']->getValues();
                $post = $this->db->table('borrow')->get($values->id);
                $post->update(['returned' => 0, 'returned_date' => NULL, 'note' => $values->note, 'qty' => $values->qty]);
                $this->db->query('INSERT INTO borrow_changes', ['account_id' => $this->user->getId(), 'borrow_id' => $values->id]);
                //$this->db->table('item_has_borrow')->where('borrow_id', $borrowId)->delete();
                $this->onSuccess($form);
        }

    }

    private function getBorrowById(){
        return $this->db->table('borrow')->get($this->id);
    }

    private function getItems(){
        $return = [];
        foreach ($this->db->table('item') as $row){
            $return[$row->id] = $row->name;
        }
        return $return;
    }


}

interface IBorrowFormFactory
{
	/**
                 * @param $id
	 * @return \Item\BorrowFormFactory
	 */
	public function create($id): BorrowFormFactory;
}
