<?php

namespace Item;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Security\Passwords;
use Nette\Application\UI\Control;
use App\Utils\Borrow\BorrowService;
use Nette\Security\User;

class ItemFormFactory extends Control
{

    /** @var \Nette\Database\Context */
    protected $db;

    /** @var \App\Utils\Borrow\BorrowService */
    private $borrowService;

    private $id;

    /** @var \Nette\Security\User */
    private $user;

    public $onError = [];

    public $onSuccess = [];

    private $defaultCondition = 6;

    public function __construct($id, Context $db, BorrowService $borrowService, User $user)
    {
        $this->id = $id;
        $this->db = $db;
        $this->user = $user;
        $this->borrowService = $borrowService;
    }

    /**
     * @return Form
     */
    protected function createComponentForm()
    {
        $form = new Form;
        $basic = $form->addContainer('basic');
        $basic->addHidden('id');
        $basic->addText('name', 'Název: ')
                ->setRequired();
        $basic->addText('label', 'Označení: ')
                ->setRequired(FALSE);
        $basic->addText('amount', 'Počet kusů: ')
                ->setType('number')
                ->setDefaultValue(1);
        $basic->addText('owned', 'Vlastněno od: ')
                ->setType('date');
        $basic->addCheckbox('end_owned', ' Již nevlastníme (zničeno, ztraceno,..)')
              ->addCondition($form::EQUAL, true)
              ->toggle('setEndDate');
        $basic->addText('end_of_ownership', 'Vlastněno do:')
             ->setType('date')
             ->setOption('id', 'setEndDate');
        $basic->addText('price', 'Cena (celé Kč): ')
                ->setType('number');
        $basic->addMultiUpload('photos', 'Fotky:')
              ->setRequired(FALSE);

        $basic->addText('placed', 'Umístění: ');
        $basic->addTextArea('description', 'Popis')
                ->setAttribute('class', 'mceEditor');
        $basic->addRadioList('condition_id', 'Stav', $this->getLabels());
        $basic->addTextArea('note', 'Poznámka (po vrácení)')
                ->setAttribute('class', 'mceEditor');
        $category = $form->addContainer('category');
        $category->addMultiSelect('item_category', 'Kategorie', $this->getCategories())->setDefaultValue($this->getItemCategories());

        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = [$this, 'save'];
        return $form;
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/formItem.latte');
        $this->template->borrows = $this->getBorrows();
        $this->template->item = $this->getItemById();
        $this->template->getFiles = function ($id){
            return $this->db->table('item_has_photo')->where('item_id', $id);
        };
        $this->template->defaultCondition = $this->defaultCondition;
        $this->template->render();
    }

    private function getBorrows(){
        return $this->borrowService->getBorrows($this->id);
    }

    public function load($id){
        $this->id = $id;
        $this->template->id = $id;
        $defaults = $this->getItemById($id)->toArray();
        $this->defaultCondition = $defaults['condition_id'];

        $this['form']['basic']->setDefaults($defaults);
        if($defaults['owned']){
          $this['form']['basic']['owned']->setDefaultValue($defaults['owned']->format('Y-m-d'));
        }
        if($defaults['end_of_ownership']){
          $this['form']['basic']['end_owned']->setDefaultValue(TRUE);
          $this['form']['basic']['end_of_ownership']->setDefaultValue($defaults['end_of_ownership']->format('Y-m-d'));
        }
    }


    public function save(Form $form)
    {
        $values = $form['basic']->getValues();
        $photos = $values->photos;
        unset($values->photos);
        if(!$values->end_owned){
          $values->end_of_ownership = NULL;
        }
        unset($values->end_owned);
        $values->owned = $values->owned == '' ? NULL : $values->owned;
        if ($values->id) {
            $post = $this->db->table('item')->get($values->id);
            $post->update($values);
            $itemId = $values->id;
        } else {
            $values->id = NULL;
            $post = $this->db->query('INSERT INTO item', $values);
            $itemId = $this->db->getInsertId();
        }
        foreach($photos as $photo){
            if($photo->getSize() > 0){
                $folder = wwwDir . "/data/item/" . $itemId;
                if(!file_exists($folder)){
                    mkdir($folder, 0777, true);
                }
                $path = $folder . "/" . $photo->getSanitizedName();
                $photo->move($path);
                $dbPath = 'data/item/' . $itemId . '/' . $photo->getSanitizedName();
                $this->db->query('INSERT INTO item_has_photo', ['src' => $dbPath, 'item_id' => $itemId]);
            }
        }
        $this->db->query('INSERT INTO item_changes', ['account_id' => $this->user->getId(), 'item_id' => $itemId]);
        $this->db->table('category_has_item')->where('item_id', $itemId)->delete();
        $categories = $form['category']->getValues();
        foreach($categories->item_category as $row){
            $this->db->query('INSERT INTO category_has_item', [
                'item_id'            =>  $itemId,
                'category_id'   =>  $row
            ]);
        }

        $this->onSuccess($form);
    }

    private function getItemById(){
        return $this->db->table('item')->get($this->id);
    }

    private function getLabels(){
        $return = [];
        $labels = $this->db->table('condition');
        foreach($labels as $row){
                $return[$row->id] = ['id' => $row->id, 'name' => $row->name, 'style' => $row->style];
        }
        return $return;
    }

    private function getCategories(){
        $return = [];
        foreach ($this->db->table('category') as $row){
            $return[$row->id] = $row->name;
        }
        return $return;
    }

    private function getItemCategories(){
        $return = [];
        $selection = $this->db->table('category_has_item')->select('category_id')->where('item_id', $this->id);
        foreach($selection as $row){
            $return[$row->category_id] = $row->category_id;
        }
        return $return;
    }



}

interface IItemFormFactory
{
	/**
                 * @param $id
	 * @return \Item\ItemFormFactory
	 */
	public function create($id): ItemFormFactory;
}
