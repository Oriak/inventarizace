<?php

namespace Item;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Security\Passwords;
use Nette\Application\UI\Control;

class ConditionFormFactory extends Control
{

        /** @var \Nette\Database\Context */
        protected $db;

        private $id;

        public $onError = [];

        public $onSuccess = [];

        public function __construct($id, Context $db)
        {
                $this->id = $id;
                $this->db = $db;
        }

        /**
         * @return Form
         */
        protected function createComponentForm()
        {
                        $form = new Form;
                        $form->addHidden('id');
                        $form->addText('name', 'Název: ')
                             ->setRequired();
                        $form->addRadioList('style', 'Štítek: ', $this->getLabels());

                        $form->addSubmit('save', 'Uložit');
                        $form->onSuccess[] = [$this, 'save'];
                        return $form;
        }

        public function render() {
                        $this->template->setFile(__DIR__ . '/formCondition.latte');
                        $this->template->render();
        }

        public function load($id){
                        $this->id = $id;
                        $this->template->id = $id;
                        $this['form']->setDefaults($this->getConditionById($id)->toArray());
        }


        public function save(Form $form)
        {
                        $values = $form->getValues();
                        if ($values->id) {
                            $post = $this->db->table('condition')->get($values->id);
                            $post->update($values);
                        } else {
                            $values->id = NULL;
                            $post = $this->db->query('INSERT INTO condition', $values);
                        }
                        $this->onSuccess($form);
        }

        private function getConditionById(){
                        return $this->db->table('condition')->get($this->id);
        }

        private function getLabels(){
                        return [
                            'success' => 'Štítek',
                            'warning' => 'Štítek',
                            'danger' => 'Štítek',
                            'secondary' => 'Štítek',
                            'primary' => 'Štítek'
                        ];
        }


}

interface IConditionFormFactory
{
	/**
                 * @param $id
	 * @return \Item\ConditionFormFactory
	 */
	public function create($id): ConditionFormFactory;
}
