<?php

namespace Category;

use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Security\Passwords;
use Nette\Application\UI\Control;

class CategoryFormFactory extends Control
{

        /** @var \Nette\Database\Context */
        protected $db;

        private $id;

        public $onError = [];

        public $onSuccess = [];

        public function __construct($id, Context $db)
        {
                $this->id = $id;
                $this->db = $db;
        }

        /**
         * @return Form
         */
        protected function createComponentForm()
        {
                        $form = new Form;
                        $basic = $form->addContainer('basic');
                        $basic->addHidden('id');
                        $basic->addText('name', 'Název kategorie: ')
                             ->setRequired();
                        $basic->addSelect('section_id', 'Sekce', $this->getSections())->setDefaultValue($this->getCategorySection());

                        $form->addSubmit('save', 'Uložit');
                        $form->onSuccess[] = [$this, 'save'];
                        return $form;
        }

        public function render() {
                        $this->template->setFile(__DIR__ . '/form.latte');
                        $this->template->render();
        }

        public function load($id){
                        $this->id = $id;
                        $this->template->id = $id;
                        $this['form']['basic']->setDefaults($this->getCategoryById($id)->toArray());
        }


        public function save(Form $form)
        {
                        $values = $form['basic']->getValues();
                        $values->section_id = $values->section_id == 'NULL' ? NULL : $values->section_id;
                        if ($values->id) {
                            $post = $this->db->table('category')->get($values->id);
                            $post->update($values);
                        } else {
                            $values->id = NULL;
                            $post = $this->db->query('INSERT INTO category', $values);
                        }
                        $this->onSuccess($form);
        }

        private function getSections(){
                $return = ['NULL' => 'Nezařazeno'];
                foreach ($this->db->table('section') as $row){
                    $return[$row->id] = $row->name;
                }
                return $return;
        }

        private function getCategorySection(){
                $return = '';
                $selection = $this->db->table('section_has_category')->select('section_id')->where('category_id', $this->id);
                foreach($selection as $row){
                    $return = $row->section_id;
                }
                return empty($return) ? NULL : $return;
        }

        private function getCategoryById(){
                        return $this->db->table('category')->get($this->id);
        }

}

interface ICategoryFormFactory
{
	/**
                 * @param $id
	 * @return \Category\CategoryFormFactory
	 */
	public function create($id): CategoryFormFactory;
}
