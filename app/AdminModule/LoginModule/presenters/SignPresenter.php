<?php

namespace App\AdminModule\LoginModule\Presenters;

use App\AdminModule\Presenters\BaseAdminPresenter;
use Nette\Application\UI\Form;
use Nette\Security as NS;
use App\Model\UserManager;

class SignPresenter extends BaseAdminPresenter
{

	/** @var \Admin\Login\ISignFormFactory @inject */
	public $signFormFactory;
        
                /** @var \App\Model\UserManager @inject */
	public $userManager;

	/** @var string @persistent */
	public $restoreKey;

	/** @var bool|mixed|\Nette\Database\Table\IRow */
	private $tokenRow;

	public function checkLogin()
	{
	}

	public function checkRights()
	{
	}

	public function actionIn()
	{
                        if ($this->user->isLoggedIn()) {
                                        $this->redirectHomepage();
                        }
	}

	public function actionLost()
	{
                        if ($this->user->isLoggedIn()) {
                                        $this->redirectHomepage();
                        }
	}

	public function actionLogout()
	{
                        $this->getUser()->logout();
                        $this->flashMessage('Odhlášení bylo úspěšné', 'success');
                        $this->redirect('in');
	}

	public function actionRenew($token)
	{
                        $row = $this->tokenProvider->valid($token)
                                        ->limit(1)
                                        ->fetch();

                        if ($row !== FALSE) {
                                        $this->tokenRow = $row;
                        } else {
                                        $this->flashMessage('Link pro obnovu hesla není platný.', 'error');
                                        $this->redirect('lost');
                        }
	}


	/**
	 * @return \Admin\Login\SignForm
	 */
	public function createComponentSignForm()
	{
                        $signForm = $this->signFormFactory->create();
                        $signForm['form']->onError[] = function(Form $form){
                                $this->formError($form);
                        };
                        $signForm['form']->onSuccess[] = function(Form $form){
                                $this->formSuccess($form);
                        };

                        return $signForm;
	}


	/**
	 * @param $form
	 */
	public function formError(Form $form)
	{
                        foreach ($form->errors as $error) {
                                        $this->flashMessage($error, 'error');
                        }

                        $this->redirect('this');
	}

	/**
	 * @param \Nette\Application\UI\Form $form
	 */
	public function formSuccess(Form $form)
	{
                        try {
                                $values = $form->getValues();
                                $identity = $this->userManager->authenticate([$values->email, $values->password]);
                                $this->user->getStorage()->setNamespace('Admin');
                                $this->user->login($identity);
                                $this->user->setExpiration('60 minutes');
                                $this->flashMessage('Přihlášení bylo úspěšné.');
                                if($this->restoreKey){
                                        $this->restoreRequest($this->restoreKey);
                                }
                                $this->redirectHomepage();
		} catch (NS\AuthenticationException $e) {
			$this->flashMessage($e->getMessage());
			$this->redirect('this');
		}
	}

}
