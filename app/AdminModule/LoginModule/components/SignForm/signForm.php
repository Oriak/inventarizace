<?php

namespace Admin\Login;

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Nette\Application\UI\ITemplate;
use Nette\Localization\ITranslator;

class SignForm extends Control
{

	/**
	 * Vytvori prihlasovaci formular
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
                        $form = new Form;

                        $form->getElementPrototype()->id = 'form-signin';

                        $form->addText('email', NULL)
                                        ->setAttribute('placeholder', 'Email')
                                        ->setRequired('Zadejte prosím email.');

                        $form->addPassword('password', NULL)
                                        ->setAttribute('placeholder', 'Heslo')
                                        ->setRequired('Zadejte prosím heslo.');

                        $form->addSubmit('makeLogin', 'Přihlásit');

                        return $form;
	}

	protected function prepareTemplate(\Nette\Application\UI\ITemplate $template)
	{
                        $this->template->setFile(__DIR__ . '/signForm.latte');
	}

                public function render()
	{
                        $template = $this->template;
                        $this->prepareTemplate($template);
                        $template->render();
	}

}

interface ISignFormFactory
{

	/**
	 * @return SignForm
	 */
	public function create(): SignForm;
}
