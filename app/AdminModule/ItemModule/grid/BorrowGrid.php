<?php

/**
 * Event grid
 */

namespace AdminModule\Item;

use Ublaboo\DataGrid\DataGrid;
use Nette\Database\Table\Selection;
use Nette\Database\Context;

class BorrowGrid
{
    /** @var \Nette\Database\Context */
    protected $db;

    public function __construct(Context $db)
  	{
  		  $this->db = $db;
  	}

  	/**
  	 * @return Grid
  	 */
  	public function create()
  	{
		    $grid = new DataGrid();
        $grid->setDataSource($this->getModel());
        $grid->setItemsPerPageList([30, 50, 100, 500, 'all']);
        $grid->setAutoSubmit(true);
        $grid->setStrictSessionFilterValues(false);

        /**
        * Columns
        */
        $grid->addColumnNumber('id', 'Id')
             ->addCellAttributes(["width" => "8%"])
             ->setSortable()
              ->setAlign('left');

        $grid->addColumnText('item_id', 'Půjčeno')
              ->setTemplate(__DIR__ . '/../../templates/grid/itemId.latte')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center']);

        $grid->addColumnText('borrowed_name', 'Komu')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center']);

        $grid->addColumnDateTime('borrowed_date', 'Od')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center'])
              ->setFormat("j. n. Y");

        $grid->addColumnDateTime('return_date', 'Do')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center'])
              ->setFormat("j. n. Y");

        $grid->addColumnText('returned', 'Vráceno')
              ->setTemplate(__DIR__ . '/../../templates/grid/returned.latte')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center']);



         /**
          * Filters
          */

         /**
          * ACtions
          */
        $grid->addAction('edit', 'Upravit', '')
                 ->setClass('btn btn-xs btn-primary');

        $grid->addAction('delete', '', '')
              ->setIcon('trash')
              ->setTitle('Delete')
              ->addAttributes(['onclick' => "return confirm('Opravdu chcete smazat vybranou položku?');"])
              ->setClass('btn btn-xs btn-danger');

		    return $grid;
    }

  	/**
  	 * @return \Nette\Database\Table\Selection
  	 */
  	private function getModel()
  	{
  		  return $this->db->table('borrow')->order('id DESC');
  	}


}
