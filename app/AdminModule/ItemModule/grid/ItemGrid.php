<?php

/**
 * Event grid
 */

namespace AdminModule\Item;

use Ublaboo\DataGrid\DataGrid;
use Nette\Database\Table\Selection;
use Nette\Database\Context;

class ItemGrid
{
    /** @var \Nette\Database\Context */
    protected $db;

    public function __construct(Context $db)
  	{
  		  $this->db = $db;
  	}

  	/**
  	 * @return Grid
  	 */
  	public function create()
  	{
		    $grid = new DataGrid();
        $grid->setDataSource($this->getModel());
        $grid->setItemsPerPageList([30, 50, 100, 500, 'all']);
        $grid->setAutoSubmit(true);
        $grid->setStrictSessionFilterValues(false);

        /**
        * Columns
        */
        $grid->addColumnNumber('id', 'Id')
             ->addCellAttributes(["width" => "8%"])
             ->setSortable()
              ->setAlign('left');

        $grid->addColumnText('name', 'Název')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center']);

        $grid->addColumnText('label', 'Označení')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center']);

        $grid->addColumnText('amount', 'Počet')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center'])
              ->setRenderer(function($item){
                return $item->amount . " ks";
              });

        $grid->addColumnText('condition_id', 'Stav')
              ->setTemplate(__DIR__ . '/../../templates/grid/condition.latte')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center']);

        $grid->addColumnText('end_of_ownership', 'Vlastněno')
              ->setTemplate(__DIR__ . '/../../templates/grid/ownership.latte')
              ->setSortable()
              ->addCellAttributes(['class' => 'text-center']);



         /**
          * Filters
          */
        $grid->addFilterText('name', 'Search', ['name']);
        $grid->addFilterText('label', 'Search', ['surname']);
        $grid->addFilterSelect('end_of_ownership', 'Search', ['' => 'Vše', 1 => 'Ano', 2 => 'Ne'])
                  ->setCondition(function(\Nette\Database\Table\Selection $db, $value) {
                      if($value == 1){
                          $db->where('end_of_ownership IS NULL');
                      }
                      elseif($value == 2){
                          $db->where('end_of_ownership IS NOT NULL');
                      }
                      else{
                          $db->where('end_of_ownership IS NOT NULL OR end_of_ownership IS NULL');
                      }
          });

         /**
          * ACtions
          */
        $grid->addAction('edit', 'Upravit', '')
                 ->setClass('btn btn-xs btn-primary');

        $grid->addAction('delete', '', '')
              ->setIcon('trash')
              ->setTitle('Delete')
              ->addAttributes(['onclick' => "return confirm('Opravdu chcete smazat vybranou položku?');"])
              ->setClass('btn btn-xs btn-danger');

		    return $grid;
    }

  	/**
  	 * @return \Nette\Database\Table\Selection
  	 */
  	private function getModel()
  	{
  		  return $this->db->table('item')->order('id DESC');
  	}


}
