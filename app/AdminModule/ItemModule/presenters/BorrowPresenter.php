<?php

namespace App\AdminModule\ItemModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Item\IBorrowTable;
use Item\IBorrowFormFactory;
use Nette\Application\UI\Form;
use Nette\Database\Context;
use AdminModule\Item\BorrowGrid;

/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class BorrowPresenter extends BaseAdminPresenter
{

    /** @var \Item\IBorrowTable */
    private $borrowTable;

    /** @var \Item\IBorrowFormFactory */
    private $borrowForm;

    /** @var \AdminModule\Item\BorrowGrid */
    private $borrowGrid;

    /** @var \Nette\Database\Context */
    protected $db;


    public function __construct(Context $db, IBorrowTable $borrowTable, IBorrowFormFactory $borrowForm, BorrowGrid $borrowGrid)
  	{
        parent::__construct();
        $this->borrowTable = $borrowTable;
        $this->borrowForm = $borrowForm;
        $this->db = $db;
        $this->borrowGrid = $borrowGrid;
  	}

  	public function startup()
  	{
        parent::startup();
  	}

    protected function createComponentTable(){
            return $this->borrowTable->create();
    }

    protected function createComponentGrid(){
        return $this->borrowGrid->create();
    }

    protected function createComponentForm()
    {
            $formAdd = $this->borrowForm->create(null);
            $formAdd->onSuccess[] = function(Form $form) use ($formAdd) {
                    if($form['basic']->getValues()->id){
                        $this->flashMessage('Výpůjčka upravena', 'success');
                        $this->redirect('Borrow:');
                    }
                    else{
                        $this->flashMessage('Výpůjčka přidána', 'success');
                        $this->redirect('Borrow:');
                    }
            };
            return $formAdd;
    }

    public function actionEdit($id)
    {
            $this->borrowForm->create($id);
            $this['form']->load($id);
    }

    public function actionDelete($id){
            $this->db->table('borrow')->where('id', $id)->delete();
            $this->flashMessage('Výpůjčka vymazána', 'warning');
            $this->redirect('Borrow:');
    }






}
