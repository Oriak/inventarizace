<?php

namespace App\AdminModule\ItemModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Item\IItemTable;
use Item\IItemFormFactory;
use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Utils\Random;
use AdminModule\Item\ItemGrid;

/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class ItemPresenter extends BaseAdminPresenter
{

    /** @var \Item\IItemTable */
    private $itemTable;

    /** @var \Item\IItemFormFactory */
    private $itemForm;

    /** @var \AdminModule\Item\ItemGrid */
    private $itemGrid;

    /** @var \Nette\Database\Context */
    protected $db;


    public function __construct(Context $db, IItemTable $itemTable, IItemFormFactory $itemForm, ItemGrid $itemGrid)
  	{
  		  parent::__construct();
        $this->itemTable = $itemTable;
        $this->itemForm = $itemForm;
        $this->itemGrid = $itemGrid;
        $this->db = $db;
  	}

  	public function startup()
  	{
  		  parent::startup();
  	}

    protected function createComponentTable(){
        return $this->itemTable->create();
    }

    protected function createComponentGrid(){
        return $this->itemGrid->create();
    }

    protected function createComponentForm()
    {
        $formAdd = $this->itemForm->create(null);
        $formAdd->onSuccess[] = function(Form $form) use ($formAdd) {
                if($form['basic']->getValues()->id){
                    $this->flashMessage('Položka upravena', 'success');
                    $this->redirect('Item:');
                }
                else{
                    $this->flashMessage('Položka přidána', 'success');
                    $this->redirect('Item:');
                }
        };
        return $formAdd;
    }

    public function actionEdit($id)
    {
        $this->itemForm->create($id);
        $this['form']->load($id);
    }

    public function actionGenerate($id){
        $this->db->table('item')->where('id', $id)->update(['token' => Random::generate()]);
        $this->flashMessage("Stránka vygenerována!", "success");
        $this->redirect('Item:edit', $id);
    }

    public function actionDelete($id){
        $this->db->table('item')->where('id', $id)->delete();
        $this->flashMessage('Položka vymazána', 'warning');
        $this->redirect('Item:');
    }

    public function actionDeletePhoto($id){
        $file = $this->db->table('item_has_photo')->where('id', $id)->fetch();
        $this->db->table('item_has_photo')->where('id', $id)->delete();
        unlink(wwwDir . '/' . $file->src);
        $this->flashMessage('Fotka smazána', 'warning');
        $this->redirect('Edit', $file->item_id);
    }





}
