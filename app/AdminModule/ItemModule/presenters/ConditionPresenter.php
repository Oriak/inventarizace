<?php

namespace App\AdminModule\ItemModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;
use App\AdminModule\Presenters\BaseAdminPresenter;
use Item\IConditionTable;
use Item\IConditionFormFactory;
use Nette\Application\UI\Form;
use Nette\Database\Context;

/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class ConditionPresenter extends BaseAdminPresenter
{
        
                /** @var \Item\IConditionTable */
                private $conditionTable;
                
                /** @var \Item\IConditionFormFactory */
                private $conditionForm;
                
                /** @var \Nette\Database\Context */
                protected $db;
        
    
                public function __construct(Context $db, IConditionTable $conditionTable, IConditionFormFactory $conditionForm)
	{
		parent::__construct();
                                $this->conditionTable = $conditionTable;
                                $this->conditionForm = $conditionForm;
                                $this->db = $db;
	}

	public function startup()
	{
		parent::startup();
	}
                
                protected function createComponentTable(){
                                return $this->conditionTable->create();
                }
                
                protected function createComponentForm()
                {
                                $formAdd = $this->conditionForm->create(null);
                                $formAdd->onSuccess[] = function(Form $form) use ($formAdd) {
                                        if($form->getValues()->id){
                                            $this->flashMessage('Stav upraven', 'success');
                                            $this->redirect('Condition:');
                                        }
                                        else{
                                            $this->flashMessage('Stav přidán', 'success');
                                            $this->redirect('Condition:');
                                        }
                                };
                                return $formAdd;
                }

                public function actionEdit($id)
                {
                        $this->conditionForm->create($id);
                        $this['form']->load($id);
                }

                public function actionDelete($id){
                                $this->db->table('condition')->where('id', $id)->delete();
                                $this->flashMessage('Stav vymazán', 'warning');
                                $this->redirect('Condition:');
                }
                
        
        
        
        
        
}
