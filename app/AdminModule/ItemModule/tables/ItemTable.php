<?php

/**
 * Item table
 */

namespace Item;

use Nette\Database\Table\Selection;
use Nette\Database\Context;
use Nette\Application\UI\Control;

class ItemTable extends Control
{
                /** @var \Nette\Database\Context */
	protected $db;

                public function __construct(Context $db)
	{
		$this->db = $db;
	}

	public function render() {
                                $this->template->setFile(__DIR__ . '/tableItem.latte');
                                $this->template->data = $this->getModel();
                                $this->template->render();
                }

	/**
	 * @return \Nette\Database\Table\Selection
	 */
	private function getModel()
	{
		return $this->db->table('item');
	}


}

interface IItemTable
{
	/**
	 * @return \Item\ItemTable
	 */
	public function create(): ItemTable;
}
