<?php

/**
 * Borrow table
 */

namespace Item;

use Nette\Database\Table\Selection;
use Nette\Database\Context;
use Nette\Application\UI\Control;

class BorrowTable extends Control
{
    /** @var \Nette\Database\Context */
	protected $db;

    public function __construct(Context $db)
	{
			$this->db = $db;
	}

	public function render() {
	        $this->template->setFile(__DIR__ . '/tableBorrow.latte');
	        $this->template->borrows = $this->getModel();
	        $this->template->today = date('Y-m-d');
	        $this->template->render();
    }

	/**
	 * @return \Nette\Database\Table\Selection
	 */
	private function getModel()
	{
			return $this->db->table('borrow')->order('id DESC');
	}


}

interface IBorrowTable
{
	/**
	 * @return \Item\BorrowTable
	 */
	public function create(): BorrowTable;
}
