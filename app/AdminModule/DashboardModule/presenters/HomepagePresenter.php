<?php

namespace App\AdminModule\DashboardModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;
use Nette\Database\Context;
use App\AdminModule\Presenters\BaseAdminPresenter;

/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
class HomepagePresenter extends BaseAdminPresenter
{
        
        /** @var \Nette\Database\Context */
	protected $db;
        
        private $matchesGridFactory;

        private $tipModel;

        private $accountModel;

        public function __construct(Context $db)
	{
		parent::__construct();
		$this->db = $db;
	}

	public function startup()
	{
		parent::startup();
                $this->template->items = $this->getItems();
                $this->template->borrows = $this->getBorrows();
                $this->template->today = date('Y-m-d');
	}
                
        private function getItems(){
                return count($this->db->table('item')->fetchAll());
        }
        
        private function getBorrows(){
                return $this->db->table('borrow')->where('returned', 0)->order('return_date ASC')->limit(10);
        }
        
        
        
        
        
}
