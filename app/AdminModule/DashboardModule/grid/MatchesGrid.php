<?php

/**
 * Event grid
 */

namespace AdminModule\Dashboard;

use Ublaboo\DataGrid\DataGrid;
use Nette\Database\Table\Selection;
use Nette\Database\Context;

class MatchesGrid 
{
        /** @var \Nette\Database\Context */
	protected $db;
        
        public function __construct(Context $db)
	{
		$this->db = $db;
	}

	/**
	 * @return Grid
	 */
	public function create()
	{
		$grid = new DataGrid();
                //$grid->setTemplateFile(__DIR__ . '/../templates/Matches/datagrid.latte');
                $grid->setDataSource($this->getModel());
                $grid->setItemsPerPageList([30, 50, 100, 500, 'all']);
                $grid->setAutoSubmit();
                    
                /**
                * Columns
                */
               $grid->addColumnNumber('id', 'Id')
                       ->addAttributes(["width" => "7%"])
                       ->setSortable()
                        ->setAlign('left');
               
               $grid->addColumnDateTime('date', 'Datum')
                       ->setSortable()
                       ->setFormat('j.n.Y H:i');
               
               $grid->addColumnText('league', 'Liga')
                    ->setSortable()
                    ->addAttributes(['class' => 'text-center'])
                    ->setRenderer(function($item) {
                        return($this->getLeagueName($item->league));
                    });
               
               $grid->addColumnText('round', 'Kolo')
                       ->addAttributes(["width" => "7%"])
                       ->setSortable()
                        ->setAlign('left');
               
               $grid->addColumnText('home_id', 'Domácí')
                    ->setSortable()
                    ->addAttributes(['class' => 'text-center'])
                    ->setRenderer(function($item) {
                        return($this->getTeamName($item->home_id));
                    });
               
               $grid->addColumnText('guest_id', 'Hosté')
                    ->setSortable()
                    ->addAttributes(['class' => 'text-center'])
                    ->setRenderer(function($item) {
                        return($this->getTeamName($item->guest_id));
                    });
               
               $grid->addColumnText('score', 'Skóre')
                    ->addAttributes(['class' => 'text-center'])
                    ->setRenderer(function($item) {
                        return (($item->score1 !== NULL ? $item->score1 : '-') . ' : ' . ($item->score2 !== NULL ? $item->score2 : '-'));
                    });
                    
                $grid->addColumnNumber('tips_count', 'Počet tipů')
                        ->addAttributes(['class' => 'text-center'])
                        ->setRenderer(function($item) {
                        return $this->getTips($item->id);
                    });
               
                    
                    
               //$grid->addFilterMultiSelect('home_id', 'Domácí:', $this->getTeams(), 'home_id');
               //$grid->addFilterMultiSelect('guest_id', 'Hosté:', $this->getTeams(), 'guest_id');
               //$grid->addFilterText('round', 'Search', ['round']); 

               /**
                * ACtions
                */
               $grid->addAction('edit', 'Upravit', '')
                       ->setClass('btn btn-xs btn-primary ajax')
                       ->setRenderer(function($item){
                           return '<a href="/admin/matches/matches/edit/'.$item->id.'" class="btn btn-xs btn-primary ajax">Upravit</a>';
                       });
               
               
               $grid->addAction('setResult', 'Zadat výsledek', '')
                       ->setClass('btn btn-xs btn-success ajax')
                       ->setRenderer(function($item){
                           return '<a href="/admin/matches/matches/set-result/'.$item->id.'" class="btn btn-xs btn-success ajax">Zadat výsledek</a>';
                       });
               
               
		return $grid;
	}

	/**
	 * @return \Nette\Database\Table\Selection
	 */
	private function getModel()
	{
		return $this->db->table('match')->where('DATE(date) = CURDATE() OR (DATE(date) <= CURDATE() AND score1 IS NULL)');
	}
        
        private function getTeamName($id){
            return $this->db->table('team')->select('name')->where('id', $id)->fetch()->name;
        }
        
        private function getLeagueName($id){
            return $this->db->table('league')->select('name')->where('id', $id)->fetch()->name;
        }
        
        private function getTips($matchId){
            return $this->db->table('tip')->where('match', $matchId)->count();
        }


}

