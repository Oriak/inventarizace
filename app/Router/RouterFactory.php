<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList();
		$router[] = $public = new RouteList('Public');
		$public->addRoute('pujcit[/<token>]', [
			'presenter' => 'Borrow',
			'action' => 'default',
			'token' => NULL
		]);
		$router[] = $admin = new RouteList('Admin');
		$admin->addRoute('<module>/<presenter>/<action>[/<id>]', [
			'module' => 'Dashboard',
			'presenter' => 'Homepage',
			'action' => 'default',
			'id' => NULL
		]);
		return $router;
	}
}
